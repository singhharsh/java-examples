
import java.io.*;
import java.util.*;

public class WordSearch {

	public static void main(String[] args) throws Exception {
		// Read a file
		File file = new File("/Users/harshs/JavaProjects/java-examples/src/passage.txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String line = null;
		
		while ((line = br.readLine()) != null) {
			String [] wordInLine = line.split("\\s+");
			for (int i=0; i < wordInLine.length; i++) {
				lines.add(wordInLine[i]);
			}
		}
		br.close();
		
		String[] words = lines.toArray(new String[lines.size()]);
		String keyWord = "after";
		
		WordSearch ws = new WordSearch();
		
		int count = ws.wordCount(words, keyWord);
		
		if (count != 0) {
			System.out.println("The word " + keyWord + " occurs "+ count + " times in the passage");
			
		} else {
			System.out.println("The word " + keyWord + " could not be found!");
		}
		
	}
	
	/*
	 * This function will search the variable "array" for the "word" and return the number of times that 
	 * word has been found.
	 */
	private int wordCount(String[] array, String word) {	
		
		//ADD your code here.
		
		return 0;
		
	}

}
